# COA -  Service de diffusion

## Auteurs
 - Robin HACAULT
 - Anthony LHOMME

## Description
Dépôt des sources du projet de service de diffusion de COA

## Lancement
Lancement de l'IHM d'exemple via la commande : java -jar [diffuseur.jar](https://bitbucket.org/Robin_Hacault/coa/src/fd61bd4bbbf8d7cca4765018e05fb329540f0701/diffuseur.jar?at=master)

## Documentation
 - [Rapport de conception](https://bitbucket.org/Robin_Hacault/coa/src/fd61bd4bbbf8d7cca4765018e05fb329540f0701/COA-Conception-HACAULT-LHOMME.pdf?at=master)
 - [Rapport de validation](https://bitbucket.org/Robin_Hacault/coa/src/fd61bd4bbbf8d7cca4765018e05fb329540f0701/COA-Validation-HACAULT-LHOMME.pdf?at=master)
