package fr.istic.m2gl.coa.diffuseur.afficheur.async;

import java.util.concurrent.Future;


/**
 * A class can implement the asynchronous Observer interface when it wants to be informed asynchronously of changes in observable subject.
 * @author LHOMME Anthony & HACAULT Robin
 * @param <T> - type of observable subject
 */
public interface IObserverAsync<T> {
	/**
	 * This method is called asynchronously whenever the observed object is changed.
	 */
	public Future<Void> update();
}
