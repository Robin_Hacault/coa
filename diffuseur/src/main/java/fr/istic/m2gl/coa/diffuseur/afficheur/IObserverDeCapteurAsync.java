package fr.istic.m2gl.coa.diffuseur.afficheur;

import fr.istic.m2gl.coa.diffuseur.capteur.ICapteurAsync;

/**
 * Specific observer for asynchronous sensors.
 * @author LHOMME Anthony & HACAULT Robin
 */
public interface IObserverDeCapteurAsync extends IObserver<ICapteurAsync> {}
