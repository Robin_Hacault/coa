package fr.istic.m2gl.coa.diffuseur.capteur;

import java.util.concurrent.Future;

import fr.istic.m2gl.coa.diffuseur.afficheur.IObserverDeCapteurAsync;

/**
 * Represent an observable object for asynchronous sensor.
 * @author LHOMME Anthony & HACAULT Robin
 */
public interface ICapteurAsync {
	/**
	 * Attach the observer to the asynchronous sensor.
	 */
	public void attach(IObserverDeCapteurAsync o);
	/**
	 * Detach the observer from the asynchronous sensor.
	 */
	public void detach(IObserverDeCapteurAsync o);

	/**
	 * Asynchronous operation for get the value of sensor.
	 */
	public Future<Integer> getValue();
}