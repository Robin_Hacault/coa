package fr.istic.m2gl.coa.diffuseur.strategy;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import fr.istic.m2gl.coa.diffuseur.afficheur.async.IObserverAsyncDeCapteur;

/**
 * Distribution of all values of the sensor at all observer.
 * @author LHOMME Anthony & HACAULT Robin
 */
public class DiffusionAtomique implements IAlgoDiffusion {
	
	private static final String NAME = "Diffusion atomique";
	
	private List<IObserverAsyncDeCapteur> listObserverCanal;
	private int currentValue = -1;

	@Override
	public String getName() {
		return NAME;
	}
	
	@Override
	public void configure(List<IObserverAsyncDeCapteur> listObserver) {
		listObserverCanal = listObserver;
	}

	@Override
	public void executeUpdate(int value) {
		currentValue = value;
		List<Future<Void>> futures = new ArrayList<>();
		// On informe tous les afficheurs
		for (IObserverAsyncDeCapteur observer : listObserverCanal) {
			Future<Void> f = observer.update();
			futures.add(f);
		}
		// On attend que tous les afficheurs aient récupéré la valeur
		for (Future<Void> f : futures) {
			try {
				f.get();
			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
			}
		}		
	}

	@Override
	public int executeGetValue(IObserverAsyncDeCapteur observer) {
		return currentValue;
	}
}
