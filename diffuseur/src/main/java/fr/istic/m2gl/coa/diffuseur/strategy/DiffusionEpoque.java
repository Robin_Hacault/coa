package fr.istic.m2gl.coa.diffuseur.strategy;

import java.util.ArrayList;
import java.util.List;

import fr.istic.m2gl.coa.diffuseur.afficheur.async.IObserverAsyncDeCapteur;

public class DiffusionEpoque implements IAlgoDiffusion {

	private static final String NAME = "Gestion par epoque";
	
	private List<IObserverAsyncDeCapteur> listObserverCanal;
	private List<IObserverAsyncDeCapteur> listObserverWaitRead; // liste des observers en attente de lecture
	private int mostRecentValue = -1;

	@Override
	public String getName() {
		return NAME;
	}
	
	@Override
	public void configure(List<IObserverAsyncDeCapteur> listObserver) {
		listObserverCanal = listObserver;
		listObserverWaitRead = new ArrayList<>();
	}

	@Override
	public void executeUpdate(int value) {
		if (mostRecentValue < value) {
			mostRecentValue = value;
		}
		// On informe tous les afficheurs
		for (IObserverAsyncDeCapteur observer : listObserverCanal) {
			if (!listObserverWaitRead.contains(observer)) { // pas d'ajout en double
				listObserverWaitRead.add(observer);
			}
			observer.update();
		}
	}

	@Override
	public int executeGetValue(IObserverAsyncDeCapteur observer) {
		int value = -1;
		if (listObserverWaitRead.contains(observer)) {
			listObserverWaitRead.remove(observer);
			value = mostRecentValue;
		}
		return value;
	}

}
