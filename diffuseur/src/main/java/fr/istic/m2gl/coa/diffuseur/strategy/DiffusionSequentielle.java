package fr.istic.m2gl.coa.diffuseur.strategy;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import fr.istic.m2gl.coa.diffuseur.afficheur.async.IObserverAsyncDeCapteur;

/**
 * Distribution of values of the sensor at all observer,
 * but some value of the sensor can be missed.
 * @author LHOMME Anthony & HACAULT Robin
 */
public class DiffusionSequentielle implements IAlgoDiffusion {

	private static final String NAME = "Diffusion sequentielle";
	
	private List<IObserverAsyncDeCapteur> listObserverCanal;
	private Map<IObserverAsyncDeCapteur, BlockingQueue<Integer>> observerCopyValue;

	@Override
	public String getName() {
		return NAME;
	}
	
	@Override
	public void configure(List<IObserverAsyncDeCapteur> listObserver) {
		listObserverCanal = listObserver;
		observerCopyValue = new HashMap<>();
	}

	@Override
	public void executeUpdate(int value) {
		// Tout les observers on lut la valeur
		if (observerCopyValue.entrySet().stream().filter(element -> !element.getValue().isEmpty()).count() == 0) {
			for (IObserverAsyncDeCapteur observer : listObserverCanal) {
				// Enregistre une copie de la valeur du capteur dans la blocking queu de l'observer
				BlockingQueue<Integer> blockingQueue;
				if (observerCopyValue.containsKey(observer)) {
					blockingQueue = observerCopyValue.get(observer);
				} else {
					// Nouvelle observer -> creation blocking queue
					blockingQueue = new ArrayBlockingQueue<>(1);
					observerCopyValue.put(observer, blockingQueue);
				}
				blockingQueue.add(value);
				
				// Maj observer
				observer.update();
			}
		}
	}

	@Override
	public int executeGetValue(IObserverAsyncDeCapteur observer) {
		int value = -1;
		BlockingQueue<Integer> blockingQueue = observerCopyValue.get(observer);
		if (blockingQueue != null) { // Changement d'aglo entre update et getValue
			try {
				value = observerCopyValue.get(observer).take();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return value;
	}
}
