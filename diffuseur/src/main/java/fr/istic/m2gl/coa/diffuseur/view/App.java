package fr.istic.m2gl.coa.diffuseur.view;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Entry point of the application.
 * @author LHOMME Anthony & HACAULT Robin
 */
public class App extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("gui.fxml"));
        primaryStage.setTitle("Diffuseur");
        primaryStage.setScene(new Scene(root, 600, 250));
        primaryStage.show();
        root.requestFocus();
        
        // Listener de fermeture de la fenetre
        primaryStage.setOnCloseRequest((event) -> System.exit(0));
    }

	public static void main(String[] args) {
        launch(args);
    }
}
