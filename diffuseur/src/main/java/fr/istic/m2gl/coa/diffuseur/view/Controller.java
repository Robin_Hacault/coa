package fr.istic.m2gl.coa.diffuseur.view;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;
import javafx.util.StringConverter;
import fr.istic.m2gl.coa.diffuseur.afficheur.Afficheur;
import fr.istic.m2gl.coa.diffuseur.capteur.Capteur;
import fr.istic.m2gl.coa.diffuseur.proxy.Canal;
import fr.istic.m2gl.coa.diffuseur.strategy.DiffusionAtomique;
import fr.istic.m2gl.coa.diffuseur.strategy.DiffusionEpoque;
import fr.istic.m2gl.coa.diffuseur.strategy.DiffusionSequentielle;
import fr.istic.m2gl.coa.diffuseur.strategy.IAlgoDiffusion;

/**
 * Controller of JavaFX application.
 * @author LHOMME Anthony & HACAULT Robin
 */
public class Controller implements Initializable  {
	
	@FXML private HBox paneAfficheur;
	@FXML private Button addAfficheur;
	@FXML private ComboBox<IAlgoDiffusion> selectAlgoDiff;
	
	private Capteur capteur;
	private ScheduledExecutorService scheduler;
	
	@Override
	public void initialize(URL location, ResourceBundle resource) {
		// Initialisation combobox pour le choix de l'algo de diffusion
		ObservableList<IAlgoDiffusion> listAlgoDiff = FXCollections.observableArrayList(
			new DiffusionAtomique(),
			new DiffusionSequentielle(),
			new DiffusionEpoque()
		);
		selectAlgoDiff.setItems(listAlgoDiff);
		selectAlgoDiff.getSelectionModel().selectFirst();
		selectAlgoDiff.setConverter(new StringConverter<IAlgoDiffusion>() {
			@Override
			public String toString(IAlgoDiffusion algoDiff) {
				return algoDiff.getName();
			}

			@Override
			public IAlgoDiffusion fromString(String name) {
				for (IAlgoDiffusion algoDiff : listAlgoDiff) {
					if (algoDiff.getName().equals(name)) {
						return algoDiff;
					}
				}
				return null;
			}
		});
		selectAlgoDiff.getSelectionModel().selectedItemProperty().addListener((selected, oldAlgoDiff, newAlgoDiff) -> handleChangeAlgoDiff(selected, oldAlgoDiff, newAlgoDiff));
		
		// Création du capteur
		capteur = new Capteur();
		capteur.setAlgoDiffusion(selectAlgoDiff.getSelectionModel().getSelectedItem());
		
		// Afficheur par defaut
		addAfficheur();

		// Thread pour les appelles à tick() 
		scheduler = Executors.newSingleThreadScheduledExecutor();
		Runnable tick = () -> capteur.tick();
		scheduler.scheduleAtFixedRate(tick, 0, 1, TimeUnit.SECONDS);
	}

	@FXML
	public void handleAddClick(Event e) {
		addAfficheur();
	}
	
	private void addAfficheur() {
		// Creation de l'afficheur IHM
		ListView<String> afficheurFx = new ListView<String>();
		ObservableList<String> listVal = FXCollections.observableArrayList();
		afficheurFx.setItems(listVal);
		paneAfficheur.getChildren().add(afficheurFx);
		
		// Creation du canal
		Canal canal = new Canal(capteur);
		capteur.attach(canal);
		
		// Creation de l'afficheur
		Afficheur afficheur = new Afficheur();
		afficheur.setObservableList(listVal);
		canal.attach(afficheur);
	}
	
	private void handleChangeAlgoDiff(ObservableValue<? extends IAlgoDiffusion> selected, IAlgoDiffusion oldAlgoDiff, IAlgoDiffusion newAlgoDiff) {
		capteur.setAlgoDiffusion(newAlgoDiff);
	}
}
