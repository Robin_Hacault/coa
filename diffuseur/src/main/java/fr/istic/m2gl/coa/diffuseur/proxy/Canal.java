package fr.istic.m2gl.coa.diffuseur.proxy;

import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import fr.istic.m2gl.coa.diffuseur.afficheur.IObserverDeCapteurAsync;
import fr.istic.m2gl.coa.diffuseur.afficheur.async.IObserverAsyncDeCapteur;
import fr.istic.m2gl.coa.diffuseur.capteur.ICapteur;
import fr.istic.m2gl.coa.diffuseur.capteur.ICapteurAsync;

/**
 * Proxy for communication between asynchronous sensor and asynchronous observer.
 * @author LHOMME Anthony & HACAULT Robin
 */
public class Canal implements ICapteurAsync, IObserverAsyncDeCapteur {
	
	private IObserverDeCapteurAsync observerAfficheur;
	
	private ScheduledExecutorService scheduler;
	private Callable<Integer> getValueCall;
	private Callable<Void> updateCall;
	
	public Canal(ICapteur capteur) {
		this.scheduler = Executors.newScheduledThreadPool(10);
		this.getValueCall = () -> capteur.getValue(this);
		this.updateCall = () -> {
			if (observerAfficheur != null) {
				observerAfficheur.update(this);
			}
			return null;
		};
	}

	@Override
	public void attach(IObserverDeCapteurAsync o) {
		observerAfficheur = o;
	}

	@Override
	public void detach(IObserverDeCapteurAsync o) {
		observerAfficheur = null;
	}

	@Override
	public Future<Void> update() {
		int delay = new Random().nextInt(5000);
		return scheduler.schedule(updateCall, delay, TimeUnit.MILLISECONDS);
	}

	@Override
	public Future<Integer> getValue() {
		int delay = new Random().nextInt(5000);
		return scheduler.schedule(getValueCall, delay, TimeUnit.MILLISECONDS);
	}
}
