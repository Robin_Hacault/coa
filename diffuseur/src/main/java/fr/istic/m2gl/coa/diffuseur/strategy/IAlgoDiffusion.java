package fr.istic.m2gl.coa.diffuseur.strategy;

import java.util.List;

import fr.istic.m2gl.coa.diffuseur.afficheur.async.IObserverAsyncDeCapteur;

/**
 * A class can implement the strategy interface when it wants to change the broadcasting algorithm.
 * @author LHOMME Anthony & HACAULT Robin
 */
public interface IAlgoDiffusion {
	/**
	 * Get the name of broadcasting algorithm.
	 * The name is unique and serves to identify algorithm.
	 */
	public String getName();
	
	/**
	 * Configure the broadcasting algorithm with an list of observer.
	 * @param listObserver - list of asynchronous observer
	 */
	public void configure(List<IObserverAsyncDeCapteur> listObserver);
	
	/**
	 * Implementation strategy of the update method.
	 * @param value - current value of sensor
	 */
	public void executeUpdate(int value);
	
	/**
	 * Implementation strategy of the getValue method.
	 * @param observer - get the value for this observer
	 * @return value - (-1) if value not exist
	 */
	public int executeGetValue(IObserverAsyncDeCapteur observer);
}
