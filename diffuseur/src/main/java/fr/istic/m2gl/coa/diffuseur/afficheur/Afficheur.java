package fr.istic.m2gl.coa.diffuseur.afficheur;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javafx.application.Platform;
import javafx.collections.ObservableList;
import fr.istic.m2gl.coa.diffuseur.capteur.ICapteurAsync;

/**
 * Concrete observer of asynchronous sensors.
 * @author LHOMME Anthony & HACAULT Robin
 */
public class Afficheur implements IObserverDeCapteurAsync {

	private Future<Integer> fVal;
	private ObservableList<String> listVal;

	/**
	 * This method set an observable list for JavaFx application 
	 * @param listVal - observable list of received values
	 */
	public void setObservableList(ObservableList<String> listVal) {
		this.listVal = listVal;
	}

	@Override
	public void update(ICapteurAsync s) {
		fVal = s.getValue();
		if (fVal != null) {
			try {
				int val = fVal.get();
				if (val != -1) {
					System.err.println("AFFICHEUR, value = "+val);
					try {
						// JavaFX thread application
						Platform.runLater(new Runnable() {
							@Override public void run() {
								listVal.add(""+val);
							}
						});
					} catch (IllegalStateException ignore) {
						listVal.add(""+val);
					}
				} // (val == -1) ignore = changement d'algo entre update & getValue
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
		}
	}
}
