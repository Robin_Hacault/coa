package fr.istic.m2gl.coa.diffuseur.afficheur.async;

import fr.istic.m2gl.coa.diffuseur.capteur.ICapteur;

/**
 * Specific asynchronous observer for sensors.
 * @author LHOMME Anthony & HACAULT Robin
 */
public interface IObserverAsyncDeCapteur extends IObserverAsync<ICapteur> {}