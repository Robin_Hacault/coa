package fr.istic.m2gl.coa.diffuseur.capteur;

import java.util.ArrayList;
import java.util.List;

import fr.istic.m2gl.coa.diffuseur.afficheur.async.IObserverAsyncDeCapteur;
import fr.istic.m2gl.coa.diffuseur.strategy.IAlgoDiffusion;

/**
 * Concrete implementation of an observable object.
 * @author LHOMME Anthony & HACAULT Robin
 */
public class Capteur implements ICapteur {
	
	private int value = 0;
	private List<IObserverAsyncDeCapteur> listObserver;
	private IAlgoDiffusion algoDiff;
	
	public Capteur() {
		listObserver = new ArrayList<>();
	}

	@Override
	public void setAlgoDiffusion(IAlgoDiffusion algo) {
		algoDiff = algo;
		algoDiff.configure(listObserver);
	}

	@Override
	public void attach(IObserverAsyncDeCapteur o) {
		listObserver.add(o);
	}

	@Override
	public void detach(IObserverAsyncDeCapteur o) {
		listObserver.remove(o);
	}

	@Override
	public int getValue(IObserverAsyncDeCapteur o) {
		int res =  -1;
		if (listObserver.contains(o)) {
			res = algoDiff.executeGetValue(o);
		}
		return res;
	}

	@Override
	public void tick() {
		value++;
		System.out.println("Tick "+value); //TODO replace by log debug
		algoDiff.executeUpdate(value);
	}
}
