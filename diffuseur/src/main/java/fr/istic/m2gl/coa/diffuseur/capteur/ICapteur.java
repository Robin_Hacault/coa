package fr.istic.m2gl.coa.diffuseur.capteur;

import fr.istic.m2gl.coa.diffuseur.afficheur.async.IObserverAsyncDeCapteur;
import fr.istic.m2gl.coa.diffuseur.strategy.IAlgoDiffusion;

/**
 * Represent an observable object for sensor.
 * @author LHOMME Anthony & HACAULT Robin
 */
public interface ICapteur {
	/**
	 * Set the values dissemination strategy.
	 * @param algo - algorithm for broadcasting
	 */
	public void setAlgoDiffusion(IAlgoDiffusion algo);
	
	
	/**
	 * Attach an asynchronous observer to the set of observers for sensor.
	 */
	public void attach(IObserverAsyncDeCapteur o);
	
	/**
	 * Detach an asynchronous observer from the set of observers of sensor.
	 */
	public void detach(IObserverAsyncDeCapteur o);
	
	
	/**
	 * Get the value of sensor.
	 * @param o - the asynchronous observer requesting the value
	 */
	public int getValue(IObserverAsyncDeCapteur o);
	
	/**
	 * Perform the sensor update to get a new value.
	 */
	public void tick();
}
