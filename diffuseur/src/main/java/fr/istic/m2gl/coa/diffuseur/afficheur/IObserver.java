package fr.istic.m2gl.coa.diffuseur.afficheur;

/**
 * A class can implement the Observer interface when it wants to be informed of changes in observable subject.
 * @author LHOMME Anthony & HACAULT Robin
 * @param <T> - type of observable subject
 */
public interface IObserver<T> {
	/**
	 * This method is called whenever the subject is changed.
	 */
	public void update(T subject);
}
