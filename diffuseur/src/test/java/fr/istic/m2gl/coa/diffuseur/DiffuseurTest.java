package fr.istic.m2gl.coa.diffuseur;

import static org.junit.Assert.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import org.junit.Before;
import org.junit.Test;

import fr.istic.m2gl.coa.diffuseur.afficheur.Afficheur;
import fr.istic.m2gl.coa.diffuseur.capteur.Capteur;
import fr.istic.m2gl.coa.diffuseur.capteur.ICapteur;
import fr.istic.m2gl.coa.diffuseur.proxy.Canal;
import fr.istic.m2gl.coa.diffuseur.strategy.DiffusionAtomique;
import fr.istic.m2gl.coa.diffuseur.strategy.DiffusionEpoque;
import fr.istic.m2gl.coa.diffuseur.strategy.DiffusionSequentielle;
import fr.istic.m2gl.coa.diffuseur.strategy.IAlgoDiffusion;

public class DiffuseurTest {
	
	private ICapteur capteur;
	private ObservableList<String> listVal1;
	private ObservableList<String> listVal2;
	
	@Before
	public void init2afficheur() {
		// 1 capteur, 2 afficheurs [SYNCHRONES]
		capteur = new Capteur();
		
		// Creation de 2 canaux
		Canal canal1 = new Canal(capteur);
		Canal canal2 = new Canal(capteur);
		// Attache les observers (canaux ASYNCHRONES) au capteur
		capteur.attach(canal1);
		capteur.attach(canal2);

		// Creation de 2 afficheurs
		Afficheur aff1 = new Afficheur();
		Afficheur aff2 = new Afficheur();
		// Attache les observers (afficheurs SYNCHRONE) aux canaux
		canal1.attach(aff1);
		canal2.attach(aff2);
		// Liste de valeur recus par l'afficheur
		listVal1 = FXCollections.observableArrayList();
		aff1.setObservableList(listVal1);
		listVal2 = FXCollections.observableArrayList();
		aff2.setObservableList(listVal2);
	}
	
	@Test
	public void testDiffusionAtomique() {
		IAlgoDiffusion algoDiff = new DiffusionAtomique();
		capteur.setAlgoDiffusion(algoDiff);

		assertEquals("Diffusion atomique", algoDiff.getName());
		
		// 3 ticks() sans interval
		capteur.tick();
		capteur.tick();
		capteur.tick();
		
		// Les 2 listes possede exactement les memes valeurs
		assertEquals(listVal1, listVal2);
		
		// Aucune valeur n'a été omise (= sujet)
		int i = 0;
		for (String val : listVal1) {
			i++;
			assertEquals(val, ""+i);
		}
	}

	@Test
	public void testDiffusionSequentielle() {
		IAlgoDiffusion algoDiff = new DiffusionSequentielle();
		capteur.setAlgoDiffusion(algoDiff);
		
		assertEquals("Diffusion sequentielle", algoDiff.getName());
		
		// 5 ticks() avec 4 secondes d'interval
		try {
			capteur.tick();
			Thread.sleep(4000);
			capteur.tick();
			Thread.sleep(4000);
			capteur.tick();
			Thread.sleep(4000);
			capteur.tick();
			Thread.sleep(4000);
			capteur.tick();
			Thread.sleep(5100); // attente max (5s) pour que tout les observers recoivent la valeur 
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		// Les 2 listes possede exactement les memes valeurs
		assertEquals(listVal1, listVal2);
		
		// La liste possede des valeurs croissante
		for (int i = 0 ; i < listVal1.size()-1; i++) {
			int val = Integer.parseInt(listVal1.get(i));
			int nextVal = Integer.parseInt(listVal1.get(i+1));
			assertTrue(val < nextVal);
		}
	}
	
	@Test
	public void testDiffusionEpoque() {
		IAlgoDiffusion algoDiff = new DiffusionEpoque();
		capteur.setAlgoDiffusion(algoDiff);
		
		assertEquals("Gestion par epoque", algoDiff.getName());
		
		// 5 ticks() avec 4 secondes d'interval
		try {
			capteur.tick();
			Thread.sleep(4000);
			capteur.tick();
			Thread.sleep(4000);
			capteur.tick();
			Thread.sleep(4000);
			capteur.tick();
			Thread.sleep(4000);
			capteur.tick();
			Thread.sleep(5100); // attente max (5s) pour que tout les observers recoivent la valeur 
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		// La liste possede des valeurs croissante (sans doublon)
		for (int i = 0 ; i < listVal1.size()-1; i++) {
			int val = Integer.parseInt(listVal1.get(i));
			int nextVal = Integer.parseInt(listVal1.get(i+1));
			assertTrue(val < nextVal);
		}

		for (int i = 0 ; i < listVal2.size()-1; i++) {
			int val = Integer.parseInt(listVal2.get(i));
			int nextVal = Integer.parseInt(listVal2.get(i+1));
			assertTrue(val < nextVal);
		}
	}
}
