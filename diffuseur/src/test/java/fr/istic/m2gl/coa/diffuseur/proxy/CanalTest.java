package fr.istic.m2gl.coa.diffuseur.proxy;

import static org.junit.Assert.assertEquals;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import org.junit.Before;
import org.junit.Test;

import fr.istic.m2gl.coa.diffuseur.afficheur.Afficheur;
import fr.istic.m2gl.coa.diffuseur.capteur.Capteur;
import fr.istic.m2gl.coa.diffuseur.capteur.ICapteur;
import fr.istic.m2gl.coa.diffuseur.strategy.DiffusionAtomique;
import fr.istic.m2gl.coa.diffuseur.strategy.IAlgoDiffusion;

public class CanalTest {

	private ICapteur capteur;
	private Canal canal;
	
	@Before
	public void init() {
		capteur = new Capteur();
		
		IAlgoDiffusion algoDiff = new DiffusionAtomique();
		capteur.setAlgoDiffusion(algoDiff);
		
		canal = new Canal(capteur);
		capteur.attach(canal);
	}
	
	@Test
	public void testAttachDetach() {
		// Creation afficheur
		Afficheur aff = new Afficheur();
		ObservableList<String> listVal = FXCollections.observableArrayList();
		aff.setObservableList(listVal);
		
		canal.attach(aff);
		
		// 3 ticks() sans interval
		capteur.tick();
		capteur.tick();
		capteur.tick();
		
		// L'afficheur a bien recu les 3 valeurs
		assertEquals(listVal.size(), 3);
		for (int i = 0; i < 3; i++) {
			assertEquals(listVal.get(i), i+1+"");
		}
		
		canal.detach(aff);
		
		// 3 ticks() sans interval
		capteur.tick();
		capteur.tick();
		capteur.tick();
		
		// L'afficheur a toujours que 3 valeurs
		assertEquals(listVal.size(), 3);
		for (int i = 0; i < 3; i++) {
			assertEquals(listVal.get(i), i+1+"");
		}
	}
}
