package fr.istic.m2gl.coa.diffuseur.capteur;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.ExecutionException;

import org.junit.Before;
import org.junit.Test;

import fr.istic.m2gl.coa.diffuseur.proxy.Canal;
import fr.istic.m2gl.coa.diffuseur.strategy.DiffusionAtomique;
import fr.istic.m2gl.coa.diffuseur.strategy.IAlgoDiffusion;

/**
 * JUnit test case for simulation of a sensor and broadcasting algorithm.
 * @author LHOMME Anthony & HACAULT Robin
 */
public class CapteurTest {

	private ICapteur capteur;
	private Canal canal1;
	private Canal canal2;
	
	@Before
	public void init() {
		capteur = new Capteur();
		
		IAlgoDiffusion algoDiff = new DiffusionAtomique();
		capteur.setAlgoDiffusion(algoDiff);
		
		canal1 = new Canal(capteur);
		canal2 = new Canal(capteur);
	}
	
	@Test
	public void testAttachDetach() throws InterruptedException, ExecutionException {
		// Attach 2 canaux
		capteur.attach(canal1);
		capteur.attach(canal2);
		
		// Les canaux ont bien recu les 3 tick()
		for (int i = 1; i < 4; i++) {
			capteur.tick();
			assertEquals((int) canal1.getValue().get(), i);
			assertEquals((int) canal2.getValue().get(), i);
		}
		// pas de quatrieme valeur
		assertEquals((int) canal1.getValue().get(), 3);
		assertEquals((int) canal2.getValue().get(), 3);
		
		capteur.detach(canal1);
		
		// Le canal1 ne recoit plus les valeurs
		// Le canal2 a 3 nouvelles valeurs
		for (int i = 4; i < 7; i++) {
			capteur.tick();
			assertEquals((int) canal1.getValue().get(), -1);
			assertEquals((int) canal2.getValue().get(), i);
		}
	}
}
